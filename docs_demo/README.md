# Getting Started

Here we see a simple markdown workflow you could establish for documentation. If you are new to markdown, this could be your very first step after which you could go on refining it to advanced setups as you learn more. 

Before proceeding, please ensure you have installed

1. [vscode](https://code.visualstudio.com/download)
2. [vscode markdown preview enhanced extension](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
3. [vscode paste image extension](https://marketplace.visualstudio.com/items?itemName=mushan.vscode-paste-image)


Extensions are optional, and you could also choose more or equivalent ones which you feel better suits you to get the job done once you get the grip of what you are doing. What I have shown below however use these extensions. 

1. Create a folder for docs (typically could be `docs` folder), and open vscode. 

![1](1_open_vscode.gif)

2. Lets create few markdown texts. 

![2](2_create_md_text.gif)

3. Add an image. Note how I use paste extension. 

![3](3_create_insert_img.gif)

4. Add some formula, both inline and centered. 

![4](4_create_formula.gif)

5. Export as html or pdf

![5](5_export_html_pdf.gif)

### Project Integration

In your project folder, create a `docs` folder, and do above steps to get started with your first documentation. Then along with other source files of your project, you could easily start doing version management for documentation as well. 

So when you release a particular set of source files as a `Release`, you could package along with it the suitable documentation as well. 

